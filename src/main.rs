#![feature(type_name_of_val)]
// #![allow(unused_variables)]
#![allow(dead_code)]

use std::{
    alloc::{alloc, dealloc, realloc, Layout},
    any::{type_name, TypeId},
    collections::{HashMap, HashSet},
    fmt::Debug,
    ptr::NonNull,
};

// TODO: Replace with the Bevy 'all_tuples' macro
macro_rules! expand {

    ($macro_name: ident, $ty: ident) => {
        $macro_name!{$ty}
    };

    ($macro_name: ident, $ty: ident, $($tt: ident),*) => {
        $macro_name!{$ty, $($tt),*}
        expand!{$macro_name, $($tt),*}
    };
}

trait Component: 'static + Debug + Send {}
impl<C: 'static + Debug + Send> Component for C {}

unsafe fn drop_generic<T>(ptr: *mut u8) {
    std::ptr::drop_in_place(ptr.cast::<T>());
}

#[derive(Debug, Default)]
struct World {
    archetype_container: ArchetypeContainer,
    entities:            Vec<EntityLocation>,
    components:          HashMap<TypeId, ComponentInfo>,
}

impl World {
    // FIXME: The length cannot be explicitly declared.
    //        The system has to be smart enough to figure this out on its own.
    // TODO: When adding multiple entities to an archetype, pre-allocate the space first (if capacity is too low).
    fn add_entity<C, I>(&mut self, components: I, len: usize) -> Vec<Entity>
    where
        C: ArchetypeAppender + ArchetypeBuilder,
        I: IntoIterator<Item = C>,
    {
        let entities: Vec<Entity> = (0..len).map(|n| Entity(n + self.entities.len())).collect();

        let mut locations =
            self.archetype_container
                .add_entity(&mut self.components, components, entities.clone());
        self.entities.append(&mut locations);
        entities
    }

    // Add a new component to an entity
    fn add_component<T: Component>(&mut self, entity: Entity, component: T) {
        let old_entity_location = self.entities.get(entity.0).unwrap();
        let new_entity_location = unsafe {
            archetype_transfer_entity::<T>(
                entity,
                *old_entity_location,
                &mut self.archetype_container,
                &self.components,
                false,
            )
        };

        self.copy_components(old_entity_location, &new_entity_location);

        unsafe {
            &self.archetype_container.archetypes[new_entity_location.archetype_id].components
                [&TypeId::of::<T>()]
                .set_unchecked(new_entity_location.index, &component as *const _ as *mut u8)
        };

        std::mem::forget(component);
        self.entities[entity.0] = new_entity_location;
    }

    // Remove a component from an entity
    fn remove_component<T: Component>(&mut self, entity: Entity) {
        let old_entity_location = self.entities.get(entity.0).unwrap();
        let new_entity_location = unsafe {
            archetype_transfer_entity::<T>(
                entity,
                *old_entity_location,
                &mut self.archetype_container,
                &self.components,
                true,
            )
        };

        self.copy_components(old_entity_location, &new_entity_location);
        self.entities[entity.0] = new_entity_location;
    }

    // Copy components for an entity from one archetype to another
    fn copy_components(&self, from_location: &EntityLocation, to_location: &EntityLocation) {
        let archetypes = &self.archetype_container.archetypes;
        let old_components = &archetypes[from_location.archetype_id].components;
        let new_components = &archetypes[to_location.archetype_id].components;

        // Always copy from the HashMap with fewer keys
        let keys = if old_components.len() < new_components.len() {
            old_components.keys()
        } else {
            new_components.keys()
        };

        for key in keys {
            let old_data = unsafe { old_components[key].get_unchecked(from_location.index) };
            let new_data = unsafe { new_components[key].get_unchecked(to_location.index) };

            unsafe {
                std::ptr::copy_nonoverlapping(
                    old_data,
                    new_data,
                    self.components[key].layout.size(),
                )
            };
        }
    }
}

unsafe fn archetype_transfer_entity<T: Component>(
    entity: Entity,
    entity_location: EntityLocation,
    archetype_container: &mut ArchetypeContainer,
    components: &HashMap<TypeId, ComponentInfo>,
    remove: bool,
) -> EntityLocation {
    let from_archetype = archetype_container.get_archetype_for_entity(&entity_location);

    let types = if remove {
        from_archetype
            .types
            .iter()
            .filter(|t| **t != TypeId::of::<T>())
            .copied()
            .collect()
    } else {
        let mut types = Vec::with_capacity(from_archetype.types.len() + 1);
        types.extend(from_archetype.types.iter());
        types.extend(Some(TypeId::of::<T>()));
        types
    };

    let to_archetype_location = archetype_container.get_or_insert(&*types, components);
    let to_archetype = archetype_container
        .archetypes
        .get_mut(to_archetype_location.0)
        .unwrap();

    EntityLocation::new(to_archetype_location.0, to_archetype.allocate(entity))
}

#[derive(Debug, Default, Copy, Clone, Ord, PartialOrd, Eq, PartialEq)]
struct Entity(usize);

/// Location of an entity within an archetype
#[derive(Debug, Copy, Clone)]
struct EntityLocation {
    archetype_id: usize,
    index:        usize,
}

impl EntityLocation {
    const fn new(archetype_id: usize, index: usize) -> Self {
        Self {
            archetype_id,
            index,
        }
    }
}

#[derive(Debug)]
struct ComponentInfo {
    name:    String,
    type_id: TypeId,
    layout:  Layout,
    drop_fn: unsafe fn(ptr: *mut u8),
}

impl ComponentInfo {
    fn new<T: Component>() -> Self {
        Self {
            name:    type_name::<T>().to_owned(),
            type_id: TypeId::of::<T>(),
            layout:  Layout::new::<T>(),
            drop_fn: drop_generic::<T>,
        }
    }
}

trait ComponentRegister {
    fn register(world: &mut HashMap<TypeId, ComponentInfo>);
}

// FIXME: `len` is never being set, so the `drop` function for stored components is never being called
// Stores raw component data
#[derive(Clone, Debug)]
struct ComponentVec {
    data:     NonNull<u8>,
    layout:   Layout,
    capacity: usize,
    len:      usize,
    drop_fn:  unsafe fn(ptr: *mut u8),
}

impl ComponentVec {
    fn new(layout: Layout, drop_fn: unsafe fn(ptr: *mut u8)) -> Self {
        Self {
            data: NonNull::dangling(),
            layout,
            capacity: 0,
            len: 0,
            drop_fn,
        }
    }

    fn from_component_info(component_info: &ComponentInfo) -> Self {
        Self::new(component_info.layout, component_info.drop_fn)
    }

    fn grow(&mut self, increment: usize) {
        let new_capacity = self.capacity + increment;
        let old_layout =
            Layout::from_size_align(self.layout.size() * self.capacity, self.layout.align())
                .unwrap();
        let new_layout =
            Layout::from_size_align(self.layout.size() * new_capacity, self.layout.align())
                .unwrap();

        if self.capacity == 0 {
            // TODO: Replace alloc with Global.allocate when it is made stable
            self.data = NonNull::new(unsafe { alloc(new_layout) }).unwrap();
        } else {
            let new_allocation =
                NonNull::new(unsafe { realloc(self.data.as_ptr(), old_layout, new_layout.size()) })
                    .unwrap();
            self.data = new_allocation;
        }

        self.capacity = new_capacity;
    }

    unsafe fn set_unchecked(&self, index: usize, value: *mut u8) {
        let ptr = self.get_unchecked(index);
        std::ptr::copy_nonoverlapping(value, ptr, self.layout.size());
    }

    unsafe fn get_unchecked(&self, index: usize) -> *mut u8 {
        self.data.as_ptr().add(index * self.layout.size())
    }
}

impl Drop for ComponentVec {
    fn drop(&mut self) {
        // There is nothing to free if our capacity is 0
        if self.capacity == 0 {
            return;
        }

        // TODO: Replace with Layout::repeat if/when it stabilizes
        let allocation = self.data.as_ptr();
        let size = self.layout.size();
        let layout = Layout::from_size_align(size * self.capacity, self.layout.align()).unwrap();

        for index in 0..self.len {
            unsafe {
                let ptr = allocation.add(size * index);
                (self.drop_fn)(ptr)
            }
        }

        unsafe { dealloc(allocation, layout) }
    }
}

#[derive(Debug)]
struct ArchetypeIndex(usize);

#[derive(Debug, Default)]
struct ArchetypeContainer {
    archetypes: Vec<Archetype>,
}

impl ArchetypeContainer {
    fn new() -> Self {
        // TODO: The container should contain a component-less archetype by default
        Self::default()
    }

    // TODO: Only accept one entity at a time. This will simplify things in the higher up abstractions.
    //       For example, the World struct can accept an array of component & entity data, preallocate some
    //       space in the archetype, then add those components via an iterator.
    fn add_entity<C, I, E>(
        &mut self,
        world: &mut HashMap<TypeId, ComponentInfo>,
        components: I,
        entities: E,
    ) -> Vec<EntityLocation>
    where
        C: ArchetypeAppender + ArchetypeBuilder,
        I: IntoIterator<Item = C>,
        E: IntoIterator<Item = Entity>,
    {
        let (index, mut archetype) =
            // TODO: Iteration can be done in parallel
            if let Some(archetype) = self
                .archetypes
                .iter_mut()
                .enumerate()
                .find(|a| C::is_match(a.1))
            {
                println!("Returning existing Archetype");
                archetype
            } else {
                println!("Creating new archetype");
                self.archetypes.push(C::build(world));
                (
                    self.archetypes.len() - 1,
                    self.archetypes.last_mut().unwrap(),
                )
            };

        C::append_components(components, &mut archetype);

        // FIXME: Slow and inefficient.
        //        There will be a better way to append the entities.
        let mut locations = vec![];
        for e in entities {
            locations.push(EntityLocation::new(index, archetype.entities.len()));
            archetype.entities.push(e);
        }

        // TODO: Get the length array and send the size info to `extend`

        locations
    }

    fn get_archetype_for_entity(&self, entity_location: &EntityLocation) -> &Archetype {
        self.archetypes
            .get(entity_location.archetype_id)
            .expect("Failed to get Archetype")
    }

    // Get or insert a new archetype
    #[allow(clippy::map_unwrap_or)]
    fn get_or_insert(
        &mut self,
        types: &[TypeId],
        components: &HashMap<TypeId, ComponentInfo>,
    ) -> ArchetypeIndex {
        let types = types.iter().copied().collect::<HashSet<_>>();
        self.archetypes
            .iter()
            .enumerate()
            .find(|(_, arch)| arch.types == types)
            .map(|(index, _)| ArchetypeIndex(index))
            .unwrap_or_else(|| {
                let components: HashMap<TypeId, ComponentVec> = types
                    .iter()
                    .map(|t| {
                        let component_info = components.get(t).unwrap();
                        (
                            *t,
                            ComponentVec::new(component_info.layout, component_info.drop_fn),
                        )
                    })
                    .collect();

                self.archetypes.push(Archetype::new(types, components));
                ArchetypeIndex(self.archetypes.len() - 1)
            })
    }
}

#[derive(Debug)]
struct Archetype {
    // Component types contained by this Archetype
    types: HashSet<TypeId>,

    // Component storage
    components: HashMap<TypeId, ComponentVec>,
    entities:   Vec<Entity>,

    capacity: usize,
    len:      usize,
}

unsafe impl Sync for Archetype {}

impl Archetype {
    fn new(types: HashSet<TypeId>, components: HashMap<TypeId, ComponentVec>) -> Self {
        Self {
            types,
            components,
            entities: Vec::default(),
            capacity: 0,
            len: 0,
        }
    }

    fn contains_component<C: Component>(&self) -> bool {
        self.types.contains(&TypeId::of::<C>())
    }

    fn contains_entity(&self, entity: Entity) -> bool {
        self.entities.contains(&entity)
    }

    fn grow(&mut self, increment: usize) {
        for vec in self.components.values_mut() {
            vec.grow(increment);
        }

        self.capacity += increment;
    }

    // Allocate space for a new entity, and return the entity location.
    // # Safety
    // Components must be allocated immediately following this function.
    // Not doing so will cause undefined behaviour!
    fn allocate(&mut self, entity: Entity) -> usize {
        self.len += 1;

        if self.len > self.capacity {
            if self.capacity == 0 {
                self.grow(1);
            } else {
                self.grow(self.capacity * 2);
            }
        }

        self.entities.push(entity);
        self.len - 1
    }

    fn set_len(&mut self, len: usize) {
        self.len = len;
        for v in &mut self.components.values_mut() {
            v.len = len;
        }
    }

    unsafe fn set_unchecked<T: Component>(&self, index: usize, value: *mut u8) {
        self.components
            .get(&TypeId::of::<T>())
            .unwrap()
            .set_unchecked(index, value);
    }

    unsafe fn get_unchecked<T: Component>(&self, index: usize) -> *mut u8 {
        self.components
            .get(&TypeId::of::<T>())
            .unwrap()
            .get_unchecked(index)
    }
}

macro_rules! impl_archetype_extend {
    ($($ty: ident),*) => {

    #[allow(non_snake_case)]
    impl<$($ty,)*> std::iter::Extend<($($ty,)*)> for Archetype
    where
        $($ty: Component,)* {

            fn extend<T>(&mut self, iter: T)
            where
                T: IntoIterator<Item = ($($ty,)*)> {

                for ($($ty,)*) in iter {

                    self.set_len(self.len + 1);

                    // TODO: Calc length beforehand
                    if self.len > self.capacity {
                        self.grow(self.len * 2);
                    }

                    unsafe {
                        $(
                            self.set_unchecked::<$ty>(self.len - 1, &$ty as *const _ as *mut u8);
                            std::mem::forget($ty);
                        )*
                    }
                }
            }
        }
    }
}

expand!(impl_archetype_extend, A, B, C, D, E, F, G, H, I);

// Used to create new archetypes at runtime
// TODO: Add a component count to allow for pre-allocation of memory
trait ArchetypeBuilder {
    fn build(world: &mut HashMap<TypeId, ComponentInfo>) -> Archetype;
}

macro_rules! impl_archetype_builder {

    ($($component: ident),*) => {

        impl<$($component: Component),*> ArchetypeBuilder for ($($component,)*) {

            fn build(world: &mut HashMap<TypeId, ComponentInfo>) -> Archetype {

                let types = [
                    $( TypeId::of::<$component>() ),*
                ].iter().cloned().collect();

                let components = [

                    $(
                        {
                            let type_id = TypeId::of::<$component>();
                            let info = world.entry(type_id).or_insert_with(ComponentInfo::new::<$component>);

                            (type_id, ComponentVec::from_component_info(&info))
                        }
                    ),*

                ].iter().cloned().collect();

                Archetype::new(types, components)
            }
        }
    }
}

expand!(impl_archetype_builder, A, B, C, D, E, F, G, H, I);

trait ArchetypeAppender: Sized {
    fn is_match(archetype: &Archetype) -> bool;
    fn append_components<I>(items: I, archetype: &mut Archetype)
    where
        I: IntoIterator<Item = Self>;
}

macro_rules! impl_archetype_appender {

    ($($component: ident),*) => {

        impl<$($component),*> ArchetypeAppender for ($($component,)*)
        where
            $(
                $component: Component,
            )*
        {

            fn is_match(archetype: &Archetype) -> bool {
                $(
                    archetype.contains_component::<$component>()
                ) && *
            }

            fn append_components<Iter>(items: Iter, archetype: &mut Archetype)
            where
                Iter: IntoIterator<Item = Self>
            {
                archetype.extend(items);
            }
        }
    }
}

expand!(impl_archetype_appender, A, B, C, D, E, F, G, H, I);

// fn test_set_unchecked() {
//     #[derive(Default, Debug)]
//     struct C1 {
//         marker: u32,
//         name:   String,
//     }
//
//     #[derive(Default, Debug)]
//     struct C2 {
//         foo: u8,
//     }
//
//     #[derive(Default)]
//     struct C3 {
//         _marker: PhantomData<f32>,
//     }
//
//     let mut archetype = <(C1, C2)>::build();
//
//     // dbg!(<(C1, C2)>::is_match(&archetype));
//
//     dbg!(&archetype);
//     archetype.grow(10);
//     dbg!(&archetype);
//
//     for i in 0..5 {
//         let foo = C1 {
//             marker: i,
//             name:   "foo".to_owned(),
//         };
//         unsafe {
//             archetype.set_unchecked::<C1>(i as usize, &foo as *const _ as *mut u8)
//         };
//     }
//
//     unsafe {
//         // Load a component and write some data to it
//         let comp = &mut *archetype.get_unchecked::<C1>(3).cast::<C1>();
//         println!("Index: {}", comp.marker);
//         comp.marker = 6;
//
//         // Load the same component, and perform a test read
//         let comp = &mut *archetype.get_unchecked::<C1>(3).cast::<C1>();
//         println!("Index: {}", comp.marker);
//     }
//
//     <_>::append_components(Some((C1::default(), C2::default())), &mut archetype);
// }

fn main() {
    #[derive(Debug)]
    struct Name(String);

    #[derive(Debug)]
    struct Age(u8);

    #[derive(Debug)]
    enum Colour {
        Red,
        Green,
        Blue,
    }

    let mut world = World::default();

    let components1 = vec![
        (Name("Foo".to_owned()), Age(42)),
        (Name("Bar".to_owned()), Age(69)),
    ];

    let components2 = Some((Name("Duck".to_owned()), Age(5)));
    let components3 = Some((Name("Meow".to_owned()), Colour::Red));

    dbg!(world.add_entity(components1, 2));
    dbg!(world.add_entity(components2, 1));
    dbg!(world.add_entity(components3, 1));

    dbg!(&world.entities);

    world.add_component(Entity(3), Age(20));
    world.add_component(Entity(0), Colour::Green);

    dbg!(&world.entities);

    world.remove_component::<Name>(Entity(1));
    world.remove_component::<Age>(Entity(2));

    dbg!(&world.entities);

    // Rough test
    let ty = &world.archetype_container.archetypes[2];
    let ty = unsafe {
        &*ty.components[&TypeId::of::<Age>()]
            .get_unchecked(1)
            .cast::<Age>()
    };

    println!("Name is: {:?}", ty);
}
